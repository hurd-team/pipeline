---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml

variables:
  # Force this to avoid taking "unreleased" from the changelog.
  # We don't have testing etc. anyway
  RELEASE: unstable

.hurd-build-before-script: &hurd-build-before-script
  - mkdir -p ${WORKING_DIR}

.hurd-build-script: &hurd-build-script
  # Disable autogeneration of dbgsym packages. (See #273)
  - |
    if echo "$SALSA_CI_DISABLE_BUILD_DBGSYM" | grep -qE '^(1|yes|true)$'; then
      export DEB_BUILD_OPTIONS="noautodbgsym ${DEB_BUILD_OPTIONS}"
    fi

  # Enter source package dir
  - cd ${WORKING_DIR}/${SOURCE_DIR}

  # If not disabled, bump package version
  - |
    if ! echo "$SALSA_CI_DISABLE_VERSION_BUMP" | grep -qE '^(1|yes|true)$'; then
      DATESTAMP=$(date +"%Y%m%d")
      sed -i -e "1 s/)/+salsaci+${DATESTAMP}+${CI_PIPELINE_IID})/" debian/changelog
    fi

  # Define buildlog filename
  - BUILD_LOGFILE_SOURCE=$(dpkg-parsechangelog -S Source)
  - BUILD_LOGFILE_VERSION=$(dpkg-parsechangelog -S Version)
  - BUILD_LOGFILE_VERSION=${BUILD_LOGFILE_VERSION#*:}
  - BUILD_LOGFILE_ARCH=${HOST_ARCH:-${BUILD_ARCH}}
  - BUILD_LOGFILE="${WORKING_DIR}/${BUILD_LOGFILE_SOURCE}_${BUILD_LOGFILE_VERSION}_${BUILD_LOGFILE_ARCH}.build"

  # Define build command
  - export BUILD_COMMAND="eatmydata dpkg-buildpackage ${HOST_ARCH:+--host-arch ${HOST_ARCH} -Pcross,nocheck} --build=${DB_BUILD_TYPE} ${DB_BUILD_PARAM}"

  # Add verbose option for timeout
  - |
    timeout_version="$(timeout --version | awk 'NR==1{print $4}')"
    if dpkg --compare-versions "$timeout_version" ge 8.29; then
      export SALSA_CI_BUILD_TIMEOUT_ARGS=" -v ${SALSA_CI_BUILD_TIMEOUT_ARGS}"
    fi

  # Print the build environment
  - printenv | sort

  # Build package
  - ( timeout ${SALSA_CI_BUILD_TIMEOUT_ARGS} ${BUILD_COMMAND} && if [ "${BUILD_TWICE}" = "true" ]; then ${BUILD_COMMAND}; fi ) |& OUTPUT_FILENAME=${BUILD_LOGFILE} filter-output
  # Do not append any diagnostics or cleanup code here. The above build line
  # often fails or timeouts, and the remaining 'script' section will not be
  # executed.

# Place all diagnostics or cleanup in the 'after_script' section, as it will run
# on both successful and failed jobs (but not on cancelled jobs).
.hurd-build-after-script: &hurd-build-after-script
  # Restore PWD to ${WORKING_DIR}
  - cd ${WORKING_DIR}
  # Clean away sources from artifact directory
  - rm -rf ${WORKING_DIR}/${SOURCE_DIR}
  # Print size of artifacts after build
  - du -sh
  # Warn if job artifacts size limit exceeded
  - |
    if [ "$(du -s | cut -f1)" -gt ${SALSA_CI_MAX_ARTIFACTS_SIZE} ]
    then
      echo -e "\e[91m WARNING: job artifacts exceed the size limit of $(( ${SALSA_CI_MAX_ARTIFACTS_SIZE} / 1024 ))MB which may prevent the job to succeed.\e[39m"
    fi
  # Keep in mind that this last diagnostics line will run only if all lines
  # above returned exit code zero.

.hurd-build-definition: &hurd-build-definition
  stage: build
  variables:
    DB_BUILD_PARAM: ${SALSA_CI_DPKG_BUILDPACKAGE_ARGS}
    DB_BUILD_TYPE: full
  script:
    # pass the RELEASE_FROM_CHANGELOG envvar to any consecutive job
    - echo "RELEASE_FROM_CHANGELOG=${RELEASE_FROM_CHANGELOG}" | tee ${CI_PROJECT_DIR}/salsa.env
    - *hurd-build-before-script
    - *hurd-build-script
  after_script:
    - *hurd-build-after-script
  dependencies:
    - extract-source
  artifacts:
    reports:
      dotenv: salsa.env

.hurd-build-package: &hurd-build-package
  extends:
    - .hurd-build-definition
    - .artifacts-default-expire

.build-package-hurd-i386: &build-package-hurd-i386
  extends:
    - .hurd-build-package
  variables:
    BUILD_ARCH: 'hurd-i386'
  tags:
    - hurd-i386

build hurd-i386:
  extends: .build-package-hurd-i386

blhc:
  extends: .test-blhc
  needs:
    - job: build hurd-i386
      artifacts: true

lintian:
  extends: .test-lintian
  needs:
    - job: build hurd-i386
      artifacts: true

.hurd-check-build-package-profiles: &hurd-check-build-package-profiles
  - |
    if [ -z "${BUILD_PROFILES}" ]; then
      echo "Error: BUILD_PROFILES variable is required"
      exit 1
    fi

.hurd-build-package-profiles-definition: &hurd-build-package-profiles-definition
  extends:
    - .hurd-build-definition
  script:
    - *hurd-check-build-package-profiles
    - *hurd-build-before-script
    - *hurd-build-script
  after_script:
    - *hurd-build-after-script

.hurd-test-build-package-profiles: &hurd-test-build-package-profiles
  extends:
    - .hurd-build-package-profiles-definition
  stage: test
  variables:
    SALSA_CI_DPKG_BUILDPACKAGE_ARGS: --build-profiles=${BUILD_PROFILES}
    BUILD_ARCH: 'hurd-i386'
  tags:
    - hurd-i386
  rules:
    - if: $SALSA_CI_ENABLE_BUILD_PACKAGE_PROFILES =~ /^(1|yes|true)$/
    - if: $SALSA_CI_DISABLE_ALL_TESTS =~ /^(1|yes|true)$/
      when: never
    - if: $SALSA_CI_DISABLE_BUILD_PACKAGE_PROFILES !~ /^(1|yes|true)$/

test-build-profiles:
  extends: .hurd-test-build-package-profiles

# vim: ts=2 sw=2 et sts=2 ft=yaml
