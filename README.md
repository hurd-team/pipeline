To use this, create debian/salsa-ci.yml containing::

```
---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml
  - https://salsa.debian.org/hurd-team/pipeline/raw/main/pipeline-jobs.yml
```

If the package cannot build on non-hurd systems, additionally set:

```
variables:
  SALSA_CI_DISABLE_BUILD_PACKAGE_AMD64: 1
  SALSA_CI_DISABLE_BUILD_PACKAGE_I386: 1
  SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1
```
